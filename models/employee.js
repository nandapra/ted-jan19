"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Company, {
        foreignKey: "companyId",
        as: "company",
      });

      this.belongsToMany(models.Tool, {
        through: "EmployeeTools",
        foreignKey: "employeeId",
        as: "tools",
      });

      this.hasMany(models.EmployeeTool, {
        foreignKey: "employeeId",
        as: "employeeTools",
      });
    }
  }
  Employee.init(
    {
      name: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Employee",
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );
  return Employee;
};
