"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("DrugUnitExpires", [
      {
        drugId: 1,
        unitId: 1,
        expireId: 1,
        quantity: 10,
      },
      {
        drugId: 1,
        unitId: 2,
        expireId: 1,
        quantity: 3,
      },
      {
        drugId: 1,
        unitId: 3,
        expireId: 1,
        quantity: 6,
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("DrugUnitExpires", null, {});
  },
};
