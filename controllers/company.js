const { Company, Employee } = require("../models");
const bcrypt = require("bcrypt");

module.exports = {
  async list(req, res) {
    try {
      const companies = await Company.findAll({
        include: [
          {
            model: Employee,
            as: "employees",
            attributes: ["id", "name", "email", "phone", "address"],
          },
        ],
      });
      res.status(200).json({
        message: "Success retrieve all company with information about employee",
        data: companies,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async createNewCompanyIncludeEmployee(req, res) {
    try {
      const dataCompany = {
        name: req.body.name,
        address: req.body.address,
      };

      const company = await Company.create(dataCompany);
      const dataEmployees = req.body.employees;
      dataEmployees.forEach(async (employee) => {
        const hash = await bcrypt.hash(employee.password, 10);
        employee.password = hash;
        console.log(employee);
        await company.createEmployee(employee);
      });

      res.status(201).json({
        message: "Success create a company with information about employee",
        data: company,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
