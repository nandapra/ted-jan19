const { Employee, Tool } = require("../models");
const bcrypt = require("bcrypt");

module.exports = {
  async list(req, res) {
    try {
      const employees = await Employee.findAll({
        include: [
          "company",
          {
            model: Tool,
            as: "tools",
            attributes: ["id", "name", "category", "url"],
            through: {
              attributes: [],
            },
          },
        ],
      });
      res.status(200).json({
        message:
          "Success retrieve all employees with information about company and tools",
        data: employees,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async create(req, res) {
    try {
      const password = req.body.password
      const passHash = await bcrypt.hash(password, 10)
      console.log(passHash)
      await Employee.create({
        name: req.body.name,
        phone: req.body.phone,
        address: req.body.address,
        email: req.body.email,
        companyId: req.body.companyId,
        password: passHash
      })
      res.status(200).json({
        message:
          "Success retrieve all employees data",
      })
    }catch(error) {
      res.status(500).json(error);
    }
  },

  async update(req, res) {
    try {
      const { id } = req.params;
      const employee = await Employee.findByPk(id);
      if (employee && employee.id) {
        if (req.body.password) {
          const hash = await bcrypt.hash(req.body.password, 10);
          const dataEmployee = {
            name: req.body.name || employee.name,
            phone: req.body.phone || employee.phone,
            address: req.body.address || employee.address,
            email: req.body.email || employee.email,
            password: hash || employee.password,
          };

          await employee.update(dataEmployee, { where: { id } });
          res.status(201).json({
            message: `Employee with ${id} updated successfully!`,
            data: employee,
          });
        } else {
          res.status(402).json({
            message:
              "Sorry, aturan mainnya password harus diganti, agak maksa nih",
          });
        }
      } else {
        res.status(401).json({
          message: `Employee with ${id} not found!`,
        });
      }
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
