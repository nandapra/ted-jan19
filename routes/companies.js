var express = require("express");
var router = express.Router();

const companies = require("../controllers/company");

// const JWTVerify = require("../middlewares/jwtverify").JWTVerify;
// router.use(JWTVerify);

const passport = require("passport");

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  companies.list
);

router.post(
  "/create-include-employee",
  companies.createNewCompanyIncludeEmployee
);

module.exports = router;
