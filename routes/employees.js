var express = require("express");
var router = express.Router();
const passport = require("passport");


const employees = require("../controllers/employee");

router.get(
    "/", 
    passport.authenticate("jwt", { session: false }),
    employees.list
);

router.post("/", employees.create);
router.put("/:id", employees.update);


module.exports = router;
