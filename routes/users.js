var express = require("express");
var router = express.Router();
const users = require("../controllers/user");

router.post("/login", users.login);

module.exports = router;
