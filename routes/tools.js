var express = require("express");
var router = express.Router();
const passport = require("passport");

const tools = require("../controllers/tool");

router.get("/", passport.authenticate("jwt", { session: false }), tools.list);

module.exports = router;
