"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeColumn("DrugUnitExpires", "createdAt");
    await queryInterface.removeColumn("DrugUnitExpires", "updatedAt");
    await queryInterface.addColumn("DrugUnitExpires", "quantity", {
      type: Sequelize.INTEGER,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.addColumn("DrugUnitExpires", "createdAt", {
      allowNull: false,
      type: Sequelize.DATE,
    });
    await queryInterface.addColumn("DrugUnitExpires", "updatedAt", {
      allowNull: false,
      type: Sequelize.DATE,
    });
    await queryInterface.removeColumn("DrugUnitExpires", "quantity");
  },
};
